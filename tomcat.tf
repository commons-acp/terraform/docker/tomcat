resource "docker_image" "tomcat_container_image" {
  name          = data.docker_registry_image.tomcat.name
  pull_triggers = [data.docker_registry_image.tomcat.sha256_digest]
  keep_locally  = true
}
resource "docker_container" "tomcat_container" {
  depends_on = [data.atn-utils_gitlab_package.war]
  image = docker_image.tomcat_container_image.latest
  name  = var.container_name

  volumes {
    host_path = var.app_data_path
    container_path = var.container_data_path
    read_only = false
  }
  upload {
    file = "/usr/local/tomcat/webapps/myservice.war"
    source = "myservice.war"
  }

  upload {
    file = "/usr/local/tomcat/webapps/config.properties"
    source = var.config_properties_path
  }

  ports {
      internal = 8080
      external = 8080
    }

  env = [
    "VIRTUAL_HOST=${var.java_virtual_host}",
    "VIRTUAL_PORT=8080",
    "VIRTUAL_PROTO=http",
    "CERT_NAME=${var.java_virtual_host}",
    "LETSENCRYPT_HOST=${var.java_virtual_host}",

    "VERSION_PROJECT=${var.app_version}",
    "contextConfigurationPath=/usr/local/tomcat/webapps/myservice/config.properties",
  ]

  networks_advanced {
    name = var.network_name
  }

}
