data "atn-utils_gitlab_package" "war" {
  repository_url = var.gitlab_package_url
  access_token = var.gitlab_api_token
  output_path = "myservice.war"
}
