variable "docker_tomcat_image" {
  default = "tomcat:8.5.37-jre8-alpine"
}
variable "container_name" {
  description = ""
}
variable "network_name" {
  description = ""
}
variable "config_properties_path" {
  description = ""
}
variable "container_data_path" {
  description = "exemple: /DATA_NAME"
}
variable "app_data_path" {
  description = "exemple: /home/docker-data/data/tomcat/DATA_NAME/data"
}
variable "gitlab_package_url" {
  description = ""
}
variable "gitlab_api_token" {
  description = ""
}
variable "java_virtual_host" {
  description = ""
}
variable "app_version" {
  description = ""
}
